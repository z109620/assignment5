void
welfordC(double *chunk_x, double *chunk_y, 
	double *M_x, double *M_y, double *V, double *C, int *L, double *ans)
{
  int i; 
  double Mean_x = *M_x;
  double Mean_y = *M_y;
  double Var = *V;
  double Cor = *C;
  int Lines = *L;
  double V_half;
  double C_half;
  
  for(i= 1; i < Lines; i++){
    V_half = (chunk_x[i] - Mean_x); //I do this because I don't want to store M[t] & M[t-1]
    C_half = (chunk_x[i] - Mean_x); //Redundant but makes for more intutitive code
  
    Mean_x = Mean_x + (chunk_x[i] - Mean_x)/i;
    Mean_y = Mean_y + (chunk_y[i] - Mean_y)/i;
  
    Var = V_half * (chunk_x[i] - Mean_x) + Var;
    Cor = C_half * (chunk_y[i] - Mean_y) + Cor;
  }

  ans[0] = Var;
  ans[1] = Cor;
  ans[2] = Mean_x;
  ans[3] = Mean_y;
} 