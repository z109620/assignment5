rm(list = ls())

library(bigmemory)

#Location of data
path <- "C:/Users/humber/Desktop/Stat_242"

f <- list.files(path = path, pattern = "combine*", full.names = TRUE)

system.time({d <-read.csv(f[1], header = TRUE, colClasses=c(rep("numeric",3)))})

system.time({d <-read.big.matrix(f[1], sep=",", header = TRUE, type = "double")})

d$diff <- head(d[3, ] - d[2, ])
d <- d[-blank]
names(d) <- c("time/fare", "time/fare")

quantile(head(d$fare), probs = seq(0, 1, .1))

lm(fare~time)
