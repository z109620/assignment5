#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\begin_modules
knitr
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing double
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Assignment 5: Big Data
\end_layout

\begin_layout Author
\noindent
Jacob Humber
\end_layout

\begin_layout Standard
\noindent
\align center
Git Repository: https://z109620@bitbucket.org/z109620/assignment4.git
\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
The goal this assignment is simple to state.
 Utilizing data set containing information on taxi operations calculate
 the deciles for the total fare less the tolls and then regression total
 fare less the tolls on trip time.
\begin_inset Foot
status open

\begin_layout Plain Layout
This data set is provided by the New York City Taxi & Limousine Commission
 and spans from 2010-2013.
 See http://publish.illinois.edu/dbwork/open-data/
\end_layout

\end_inset

 The challege is that there is 15GBs of data.
 To accomplish this, I will adopt two approaches.
 First, I will use UNIX commands to extract only the essential data.
 After doing so the data set is a modest 1.8 GB and can be loaded into R.
 Second, load in sequential chunks of code.
\end_layout

\begin_layout Section
\noindent
Data Cleaning
\end_layout

\begin_layout Standard
\noindent
Before I analysis the data, it is important to make sure that the data is
 clean.
 Two different types of data cleaning will be done.
 First, I will test that the rows in the fare data match those in the trip
 data.
 If this is the case then I can simply bind these two data sets together
 rather than have to merge the two data sets conditional on the key (i.e.
 unique identifier).
 Second, I will remove implausible values of either total amount less toll
 amount (greater than $5,000), trip time (greater than 18 hours) as well
 as all negative values.
 All code discussed in this section is contained in appendix 1
\end_layout

\begin_layout Subsection
Does the Data Match?
\end_layout

\begin_layout Standard
The strategy which I use to check that the rows of the fare and trip data
 match is different than the one discussed in class, which utilized connections.
 Instead, I loop over chunks of data, and use vectorized operations to check
 whether the data match.
 This approach is relatively quick (~5 mins) and checks all the data.
 The key includes the taxi medaillion and pickup date/time.
 The medallion are unique to the taxi cab.
 Hack license, which are unique the drive, is therefore not necesssary.
 
\end_layout

\begin_layout Standard
Fortunately, the fare and trip data match! To combine the data set I use
 the shell commands below.
 These shell commands first combines each of the 12 trip and fare data set
 to on another.
 The final shell commands combine the trip and fare data.
\end_layout

\begin_layout Subsection
Cleaning the Data
\end_layout

\begin_layout Standard
\noindent
Ideally, I would spend more time cleaning the data, however, time did not
 permit.
 If, I did have more time, I would attempt to understand the source of these
 implausible values, by examining the data more closely as well as looking
 for patterns.
 The webpage containing the data 
\begin_inset Foot
status open

\begin_layout Plain Layout
http://publish.illinois.edu/dbwork/open-data/
\end_layout

\end_inset

, does provide some guidance.
 For example, they mention that some values are in minutes rather than in
 seconds.
 
\end_layout

\begin_layout Section
Approach 1: Doing it all at once
\end_layout

\begin_layout Standard
After using the UNIX commands in appendix 1 below, the data set is 1.8 GBs.
 Clearly, this is small enough to load into RAM (my machine has 8GB of RAM)
 and therefore this data can be manipulated in R directly.
 To facilitate analysis of this data set I utilize the bigmemory packages.
 The results for both the deciles as well as the simple regression of trip
 time on total fare less tolls is as follows.
 All code in this section is contained in appendix 2.
\end_layout

\begin_layout Standard
Taxi cab rates are as follows
\begin_inset Foot
status open

\begin_layout Plain Layout
http://nyc.taxiwiz.com/fare.php?lang=en
\end_layout

\end_inset

: The intial charge is $3.00.
 When stopped in traffic, the rate is $.40 per minute (or $.0066 per second),
 else it is $2.00 per mile.
 These rates correspond nicely to the regression coefficents.
 The intercept is $2.44, which is only $.6 short of the $3.00 charge
\begin_inset Foot
status open

\begin_layout Plain Layout
We could likely improve this estimate along with the slope estimate if we
 accounted for the fact that this data is censored as zero.
 This can be done with a Tobit model.
 Unfortunetaly the tobit estimator, like numerous other generalized linear
 model, is not a closed form solution.
 Consequently, it would not be possible to use a tobit model in the forecoming
 approach.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
The computational time to merge the data together with Shell commands took
 ~40 minutes.
 Additionally, reading the data into R with write.big.memory took 2 minutes.
 Calculating the deciles and regressions coefficents took 10 secs and BLANK
 respecitively.
 This approach took a considerable amount of computer time.
 However the benefit of this approach is the programming ease.
 After some rather minor shell commands it is possible to load the data
 into R and use R to do the statistical calculations.
 It does not get much easier than that.
 
\end_layout

\begin_layout Standard
The main drawback of this approach is that it will not always work.
 If the data set was larger, or if the number of variables were more numerous,
 it is not likely the series of shell commands could reduce the data down
 to such a managable size.
 That being said, this approach underscores the power of the command line.
 
\end_layout

\begin_layout Section
Approach 2: Doing it in Chunks with R and C
\end_layout

\begin_layout Standard
When confronted with a large data set, the simplest way to analysis this
 data R, is to load a chunk of data, calculate the essential estimates,
 delete the chunk, load another chunk of data, update the estimates, etc.
 In the current situation, I will calculate the median by updating a frequency
 table.
 To calculate the regression, I will use the Welford's method.
 All code for this section is contained in appendix 3.
\end_layout

\begin_layout Subsection
Obtaining the Deciles
\end_layout

\begin_layout Standard
In the current deciles are calcuated using the following stragtey which
 Duncan outlined in class.
 I load in a chunk of data, generate a frequency table, load a new chunk,
 update the frequency table, etc.
 Once I have the final frequency table I can use it to obtain the deciles.
 The reason why this works is the current application is because the fare
 are in dollars.
 Consequenlty it is not possible have more than two signficant digits.
 This strategy, while very clever, will not always work.
 In cases where the data can take on any concievable numeric value the well
 known algorithm quicksort, should be utilized.
 The results for from this approach are not reported below because they
 are exactly the same those in section 2.
 Intutively, this is the case, because we are implictly doing the same thing.
 Both approachs run estimates on the whole data set, the second approach,
 however, only analyses the data one chunk at a time.
 6,7.5,8.5,9.75,11,13,15,18.5,26.12
\end_layout

\begin_layout Standard
An important take away from this approach is the usefulness of representing
 data in frequency tables.
 Utilizing frequency tables can reduce memory needs.
 In this approach my frequency table was a mere .3 MB.
\end_layout

\begin_layout Subsection
Obtaining Simple Regression 
\end_layout

\begin_layout Standard
Unfortunatley, the strategy of updating a frequency table is not feasible
 in the current application.
 By introducing an additional variable, the size of the frequency increase
 dramatically.
 For example there are ~1.7e4 observations in the frequency table in the
 previous example.
 Consequently, the a rough guess of the number of observations in a frequency
 table containing both fare and time information would be 1.7e8 = (1.7e4)^2.
\end_layout

\begin_layout Standard
Consequently the regression coefficients will be calcuated using Welford's
 method.
 In the current context, the regression contains only one regressor (i.e.
 dependent variable).
 Consequently, the regression parameter are
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
y=\beta_{0}+\beta_{1}x+e
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\hat{\beta}_{1}=\frac{Cov(x,y)}{Var(x)}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\hat{\beta}_{0}=\bar{y}-\hat{\beta}\bar{x}
\]

\end_inset


\end_layout

\begin_layout Standard
Above, 
\begin_inset Formula $Cov(\,)$
\end_inset

 and 
\begin_inset Formula $Var(\,)$
\end_inset

 represent the covaranance and varance.
 Additionally, 
\begin_inset Formula $\bar{y}$
\end_inset

 and 
\begin_inset Formula $\bar{x}$
\end_inset

 represent the sample means.
 The Welford's method, is a stable and efficient online alogrithim which
 can be utilized to obtain both the variance and covariance of the data.
 Unfortunatley, the online algorithm updates both the covarance and varance
 one an observation be observation basis.
 Consequenlty, the Welford's method necessiates that we loop, observation
 by observation, over the entire data set.
 It is well know that compuations of this nature are very slowly in R.
 Consequently, I have written Welford's algoritm in C and then interface
 with R.
 This will dramatically decrease the computaional time.
\end_layout

\begin_layout Standard
Another very important take away from this approach is the usefulness of
 interfacing with C.
 It is true that it is in general harder to write C code than R code.
 That withstanding, C code is much faster, and therefore it with the headaches,
 especially for easy alogrithm such as Welford's
\end_layout

\begin_layout Subsection
Timing
\end_layout

\begin_layout Standard
Obtaining the deciles for this approach took a mere 6 minutes.
 Clearly a drastic improvement over the previous approach.
 The time spend programming was also relatively minimal.
 The code in appendix 2 below is straightforward and easy to write.
 In terms of obtaining the deciles, this approach strictly dominates approach
 1.
 
\end_layout

\begin_layout Standard
I was not able to time the C code
\begin_inset Foot
status open

\begin_layout Plain Layout
My slow 32-bit window machine was too slow to run the code.
 I therefore attempted to run the code on my much faster lab computers.
 However the .dll, which worked on my home computer would not dynamically
 load on the 64 bit lab computers.
 After much nagging, I got our IT personnel to download Rtools on the lab
 machines.
 However, due to security constraints I was still not able to run R tools
 from the command prompt and therefore was not able to compile the code
 on a 64 bit machine.
 I tried compiling the code outside of R at the command prompt in Cygwin.
 However, I could not get this to work as well.
 The lessons I learned: First buy a new computer.
 Second, buy a Mac or a machine that runs Linux.
\end_layout

\end_inset

That withstanding, I did run the C code on my slower home computer
\begin_inset Foot
status open

\begin_layout Plain Layout
All other reported times represent computational times from my much faster
 lab machines
\end_layout

\end_inset

, and for small subsamples of data, 1e5, it ran fast ~1 min.
 The time spend programming in C, unlike with the deciles which was coded
 in R, is non-trivial.
 C is much more finicky than R.
 Consequently, bugs abound in C code and debugging takes time, a lot of
 time.
 As I learned the hard way, even getting the C code to compile can be a
 challenge.
\end_layout

\begin_layout Standard
In hindsight,
\end_layout

\begin_layout Section
Conclusion and Steps Forward
\end_layout

\begin_layout Standard
In the assignment we have utilized by two approaches to answer the question.
 The first utilized UNIX commands maniuplate the data before loading the
 data into R with the bigmemory package.
 R was utilized to analysis the data.
 The second approach analysed the data in chunks rather than all at once
\end_layout

\begin_layout Standard
The project was extremely fun! I did run out of time though.
 I was nearly done with a Bag of Little Bootstraps approach that used parallel
 computing.
 Over the summer I plan to finish this assignment as well as begin to play
 with SQL, Hadoop, Spark, etc...
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
newpage
\end_layout

\end_inset


\end_layout

\begin_layout Section*
Appendix 1: Matching and Cleaning Data
\end_layout

\begin_layout Standard
Unix Commands
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

<<echo=TRUE, eval=FALSE>>=
\end_layout

\begin_layout Plain Layout

#For the trip data
\end_layout

\begin_layout Plain Layout

cut -f 9 -d , trip_data_1.csv | cat > combine_time.csv & #first data with
 header
\end_layout

\begin_layout Plain Layout

for f in `find trip_data_[2-9].csv trip_data_[1-9][0-9].csv`
\end_layout

\begin_layout Plain Layout

do
\end_layout

\begin_layout Plain Layout

cut -f 9 -d , $f | sed 1d | cat >> combine_time.csv & #removing header
\end_layout

\begin_layout Plain Layout

done
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

#For the trip data
\end_layout

\begin_layout Plain Layout

cut -f 10,11 -d , trip_fare_1.csv | cat > combine_fare.csv & #first data with
 header
\end_layout

\begin_layout Plain Layout

for f in `find trip_fare_[2-12].csv`
\end_layout

\begin_layout Plain Layout

do
\end_layout

\begin_layout Plain Layout

cut -f 10,11 -d , $f | sed 1d | cat >> combine_fare.csv & #removing header
\end_layout

\begin_layout Plain Layout

done
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

#Joining Together
\end_layout

\begin_layout Plain Layout

paste -d , combine_time.csv combine_fare.csv > combine_all.csv &
\end_layout

\begin_layout Plain Layout

@
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Cleaning
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

<<echo=TRUE, eval=FALSE>>=
\end_layout

\begin_layout Plain Layout

rm(list = ls())
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

library(bigmemory)
\end_layout

\begin_layout Plain Layout

library(biglm)
\end_layout

\begin_layout Plain Layout

library(biganalytics)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

#Location of data
\end_layout

\begin_layout Plain Layout

path <- "/tmp/jacob"
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

f <- list.files(path = path, pattern = "combine*", full.names = TRUE)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

system.time({d <-read.big.matrix(f[1], sep=",", header = TRUE, type = "double")})
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

d <- cbind(d[ ,1], d[ ,3]-d[ ,2]) #Lessing tolls from total fare amount
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

colnames(d) <- c("time", "fare")
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

#Cleaning Time
\end_layout

\begin_layout Plain Layout

d <- d[-which(d[,"time"] < 0 | d[,"time"] > 64800), ] #deleting negative
 and >18 hours
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

#Cleaning Fare 
\end_layout

\begin_layout Plain Layout

d <- d[-which(d[,"fare"] < 0 | d[,"fare"] > 5000), ] #deleting negative
 and >18 hours
\end_layout

\begin_layout Plain Layout

@
\end_layout

\end_inset


\end_layout

\begin_layout Section*
Appendix 2: Approach 1
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

<<echo=TRUE, eval=FALSE>>=
\end_layout

\begin_layout Plain Layout

#I include this analysis in the clearning script above, so I don't have
 to reload the data
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

decile <- quantile(d[,"fare"], probs = seq(.1, .9, .1))
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

fit <- biglm(fare~time, data = as.data.frame(d))
\end_layout

\begin_layout Plain Layout

@
\end_layout

\end_inset


\end_layout

\begin_layout Section*
Appendix 3: Approach 2
\end_layout

\begin_layout Standard
Code to obtain deciles
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

<<echo=TRUE, eval=FALSE>>=
\end_layout

\begin_layout Plain Layout

path <- "C:/Users/humber/Desktop/Stat_242"
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

library(data.table)
\end_layout

\begin_layout Plain Layout

get.f.table <- function(path){
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  f <- list.files(path=path, pattern = "trip_fare_*", full.names=TRUE) 
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  for (i in 1:12){
\end_layout

\begin_layout Plain Layout

    if(i==1){ # I need to make an initial f.table
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      d <- fread(f[i], #Loading the Data
\end_layout

\begin_layout Plain Layout

                 header=TRUE, 
\end_layout

\begin_layout Plain Layout

                 sep=",",  
\end_layout

\begin_layout Plain Layout

                 select = c(10, 11),
\end_layout

\begin_layout Plain Layout

                 )
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      d$diff <- d[ , 2, with = FALSE] - d[ , 1, with = FALSE] #total less
 tolls
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      f.table <- d[ , .N, by=diff] #freq table
\end_layout

\begin_layout Plain Layout

    }
\end_layout

\begin_layout Plain Layout

    else{# merge to the initial f.table
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      d <- fread(f[i], 
\end_layout

\begin_layout Plain Layout

                 header = TRUE, 
\end_layout

\begin_layout Plain Layout

                 sep = ",",  
\end_layout

\begin_layout Plain Layout

                 select = c(10, 11)
\end_layout

\begin_layout Plain Layout

                 )
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      d$diff <- d[ , 2, with = FALSE] - d[ , 1, with = FALSE] #total less
 tolls
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      f.table.2 <- d[ , .N, by=diff]
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      test.m <- merge(f.table, f.table.2, by="diff", all=TRUE)
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      test.m$freq <- rowSums(test.m[ , 2:3, with = FALSE], na.rm = TRUE)
\end_layout

\begin_layout Plain Layout

      
\end_layout

\begin_layout Plain Layout

      f.table <- test.m[, c("diff","freq"), with = FALSE]
\end_layout

\begin_layout Plain Layout

    }
\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

  return(f.table)
\end_layout

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

get.decile <- function(data, freq, decile){
\end_layout

\begin_layout Plain Layout

  max( data[ cumsum(freq) < sum(freq) * decile ] )
\end_layout

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout

@
\end_layout

\end_inset


\end_layout

\begin_layout Standard
C Code
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

<<echo=TRUE, eval=FALSE>>=
\end_layout

\begin_layout Plain Layout

void
\end_layout

\begin_layout Plain Layout

welfordC(double *chunk_x, double *chunk_y, 
\end_layout

\begin_layout Plain Layout

	double *M_x, double *M_y, double *V, double *C, int *L, double *ans)
\end_layout

\begin_layout Plain Layout

{
\end_layout

\begin_layout Plain Layout

  int i; 
\end_layout

\begin_layout Plain Layout

  double Mean_x = *M_x;
\end_layout

\begin_layout Plain Layout

  double Mean_y = *M_y;
\end_layout

\begin_layout Plain Layout

  double Var = *V;
\end_layout

\begin_layout Plain Layout

  double Cor = *C;
\end_layout

\begin_layout Plain Layout

  int Lines = *L;
\end_layout

\begin_layout Plain Layout

  double V_half;
\end_layout

\begin_layout Plain Layout

  double C_half;
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  for(i= 1; i < Lines; i++){
\end_layout

\begin_layout Plain Layout

    V_half = (chunk_x[i] - Mean_x); //I do this because I don't want to
 store M[t] & M[t-1]
\end_layout

\begin_layout Plain Layout

    C_half = (chunk_x[i] - Mean_x); //Redundant but makes for more intutitive
 code
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

    Mean_x = Mean_x + (chunk_x[i] - Mean_x)/i;
\end_layout

\begin_layout Plain Layout

    Mean_y = Mean_y + (chunk_y[i] - Mean_y)/i;
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

    Var = V_half * (chunk_x[i] - Mean_x) + Var;
\end_layout

\begin_layout Plain Layout

    Cor = C_half * (chunk_y[i] - Mean_y) + Cor;
\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  ans[0] = Var;
\end_layout

\begin_layout Plain Layout

  ans[1] = Cor;
\end_layout

\begin_layout Plain Layout

  ans[2] = Mean_x;
\end_layout

\begin_layout Plain Layout

  ans[3] = Mean_y;
\end_layout

\begin_layout Plain Layout

}  
\end_layout

\begin_layout Plain Layout

@
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Wrapper function for C
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

<<echo=TRUE, eval=FALSE>>=
\end_layout

\begin_layout Plain Layout

wrapper = function(x,y, path.dll,mean_x, mean_y, var_x, cov_xy){
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  dyn.load(paste0(path.dll,"/welford.dll"))
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  l <- as.integer(nrow(x))
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  welford <- .C('welfordC', as.double(x), as.double(y), as.double(mean_x),
\end_layout

\begin_layout Plain Layout

	as.double(mean_y), as.double(var_x), as.double(cov_xy), l, matrix(as.double(0),4,1)
)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  results <- list(rep("NULL",4))
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  results[[1]][1:2] <- welford[[8]][1:2]/(l-1)
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  results[[1]][3:4] <- welford[[8]][3:4])
\end_layout

\begin_layout Plain Layout

  
\end_layout

\begin_layout Plain Layout

  return(results)
\end_layout

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

get.coeff <- (var_x, cov, mean_x, mean_y){
\end_layout

\begin_layout Plain Layout

  B1 <- cov/var_x
\end_layout

\begin_layout Plain Layout

  B0 <- mean_y-B1*mean_x
\end_layout

\begin_layout Plain Layout

  return(list(B1,B0))
\end_layout

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout

@
\end_layout

\end_inset


\end_layout

\end_body
\end_document
