library(data.table)

path <- "C:/Users/Athena/Google Drive/Courses/Taken/STAT_242/Assignment5"

f <- list.files(path = path, pattern = "+x")#, full.names = TRUE)

setwd(path)

l <- as.numeric(strsplit(system(paste("wc -l", f[1]), intern = TRUE), " ")[[1]][1])

n = l

b = l/2

d <- fread(f[1], select = 10, header = FALSE)


set.seed(123)
bag <- d[sample(n ,b ,replace = FALSE) ,.N, by = "V10"] #use prob to gen sample weights

set.seed(123)
bag2 <- d[sample(n ,b ,replace = FALSE)] #use prob to gen sample weights

set.seed(123)
bs <- sample(bag$V10, n, replace = TRUE, prob = bag$N/b)

set.seed(123)
bs2 <- sample(bag2$V10, n, replace = TRUE)

all(bs==bs2)
any(bs==bs2)
all(bag==bag2[ ,.N,by="V10"])